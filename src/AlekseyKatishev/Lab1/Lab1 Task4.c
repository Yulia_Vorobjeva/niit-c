#define _CRT_SECURE_NO_WARNINGS
/*
�� ����� � �����
*/
#include <stdio.h>
#include <string.h>

int main(void)
{

	int foot, inch;
	float result;
	int truefoot = 0, trueinch = 0;



	while ((truefoot == 0) || (trueinch == 0))
	{
		printf("Enter foot \n");
		scanf("%d", &foot);
		if (foot >= 0)
			truefoot = 1;
		else
		{
			printf("error foot < 0 \n");
			truefoot = 0;
			trueinch = 0;
		}

		printf("Enter inch \n");
		scanf("%d", &inch);
		if (inch >= 0)
			trueinch = 1;
		else
		{
			printf("error inch < 0 \n");
			truefoot = 0;
			trueinch = 0;
		}
	}

	result = ((foot * 12) + inch) * 2.54;
	printf("%d foot %d inch = %.1f centimetr\n", foot, inch, result);

}