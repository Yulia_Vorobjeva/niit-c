#include <stdio.h>


int str_cmp(char* str1, char* get)
{
    for(int i = 0; (str1[i] != '\0') || (get[i] != '\0') ; i++)
    {
        if (str1[i] != get[i])
        {
            return 1;
        }    
    }
    return 0;
}

void print_result(int res, int g_res)
{    
     char mes[][20] = {"Fat", "Thin", "Normal"};
     
     if ( res < g_res)
        {
            printf("Result: %s\n", mes[0]);
        }
        else if( res > g_res)
        {
            printf("Result: %s\n", mes[1]);
        }
        else if(res == g_res)
        {
            printf("Result: %s\n", mes[2]);
        }   
}

int main(void)

{   
    char gen[256];
    unsigned int h = 0;
    unsigned int w = 0;
   
    
    printf("Your gender?\n");
    scanf("%s", gen);
    
    printf("Your height?\n");
    scanf("%i", &h);
    
    printf("Your weight?\n");
    scanf("%i", &w);
    
    int res = (h-w);
    int hum_factor = 0;

    if (str_cmp("male", gen) == 0) 
    {
        hum_factor = 100;
    }
    else if (str_cmp("female", gen) == 0)
    {
        hum_factor = 110;
    }
    
    print_result(res, hum_factor);

    return 0;
}

