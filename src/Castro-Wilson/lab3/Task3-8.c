/********************************************************************
 * Программа, которая запрашивает у пользователя строку, состоящую  *
 * из нескольких слов и целое число n, а затем выводит n - ое слово *
 * строки на экран. В случае неккоректного n выводится сообщение об *
 * ошибке/                                                          *
 * By Wilson Castro. 11.2016.                                       *
 ********************************************************************/
#include <stdio.h>
#include <string.h>
#define SIZE 256
#define TRUE 1
#define FALSE 0

int main() {
    char line[SIZE] = {0};
    int i, j;
    int k = 0;
    int num = 0;
    int cuonter = 0;
    int firstLetter = 0;
    int secondLetter = 0;
    int firstWord = FALSE;
    int secundWord = FALSE;

    printf("Введите строку для обработки\n");
    fgets(line, 256, stdin);
    printf("Введите целое число не превышающее количество слов в строке\n");
    scanf("%d", &num);
    j = strlen(line);
    line[j - 1] = '\0';

    for (i = 1; i < j; i++) {
        if (i - 1 == 0 && line[i - 1] != ' ') { // пойск первой буквы первого слово 
            firstWord = TRUE;
            firstLetter = i - 1;
        } else
            if (line[i - 1] == ' ' && line[i] != ' ') { // пойск первой буквы первого слово если до него есть пробел
            firstWord = TRUE;
            firstLetter = i;
        }
        if (line[i - 1] != ' ' && line[i] == ' ') { //цикл для пойска последной буквы в случае окончаяния 
            cuonter++; // слова с пробелом     
            secundWord = TRUE;
            secondLetter = i - 1;
        }
        if (line[i - 1] != ' ' && line[i] == '\0') { //цикл для для пойска последной буквы в случае окончаяния 
            cuonter++; // слова и строка 
            secundWord = TRUE;
            secondLetter = i - 1;
        }

        if (firstWord == TRUE && secundWord == TRUE) { // цикл для ввывода n-ое слова   
            for (k = firstLetter; k <= secondLetter; k++) {  
                for(;  cuonter==num;){
                if (!line[k] != ' ') {
                    printf("%c", line[k]);
                }
                break;
                }
            }
            firstWord = FALSE;
            secundWord = FALSE;
            firstLetter = 0;
            secondLetter = 0;
        }
    }
    if(num>cuonter){
    printf("Ошибка! Число превышает количество слов в строки");
    }
    return 0;
}
