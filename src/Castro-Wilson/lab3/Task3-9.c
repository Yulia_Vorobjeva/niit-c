/******************************************************************
 * Программа, определяющая в строке символьную последовательность *
 * максимальной длины.                                            *
 * Замечание:                                                     *
 * Для строки AABCCCDDEEEEF выводится 4 и EEEE                    *
 * By Wilson Castro. 12.2016.                                     *
 ******************************************************************/
#include <stdio.h>
#include <string.h>
#define SIZE 256
#define SIZE2 127

int main() {
    int j = 0;
    int i = 0;
    int t = 0;
    int count = 0; //счетчик частоты повторения символов
    int biggest = 0;
    int moda = 0;
    char line[SIZE] = {0}; // массив для сохранения введеной строки
    int frecuency[SIZE2] = {0}; // массив для сохранения частоты повторов символов
    char simbols[SIZE2] = {0};

    printf("Введите строку\n");
    fgets(line, 256, stdin);
    j = strlen(line);
    line[j - 1] = '\0';
    for (i = 32; i <= SIZE2; i++) {
        simbols[i] = i;
    }
    for (count = 0; count <= SIZE; count++) { // счетчик частоты повторения символа
        ++frecuency [line[count]];
    }
    for (i = 0; i <= SIZE2; i++) { // ищет моду элементов
        for (t = 32; t <= SIZE2 - 1; t++) {
            if (frecuency[t] > biggest) {
                biggest = frecuency[t];
                moda = t;
            }
        }
    }
    printf("%d ", biggest);
    for (i = 0; i < biggest; i++) {
        printf("%c", moda);
    }
    return 0;
}
