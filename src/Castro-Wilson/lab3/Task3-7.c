/******************************************************************
 * Программа, которая печатает таблицу встречаемости символов для *      
 * введённой строки, отсортированную по убыванию частоты.         *
 * Замечание:                                                     *
 * Таблица выводится таким образом, что сначала идут самые        *
 * многочисленные символы, а затем по мере убывания               *
 * By Wilson Castro. 12.2016.                                     *
 ******************************************************************/
#include <stdio.h>
#include <string.h>
#define SIZE 256
#define SIZE2 127

int main() {
    int j = 0;
    int i = 0;
    int count = 0; //счетчик частоты повторения символов
    int caracter = 0;
    char line[SIZE] = {0}; // массив для сохранения введеной строки
    int frecuency[SIZE2] = {0}; // массив для сохранения частоты повторов символов
    char simbols[SIZE2] = {0};
    int tempFrec = 0;
    int tempSimb = 0;

    printf("Введите строку\n");
    fgets(line, 256, stdin);
    j = strlen(line);
    line[j - 1] = '\0';
    for (i = 32; i <= SIZE2; i++) {
        simbols[i] = i;
    }
    for (count = 0; count <= SIZE; count++) { // счетчик частоты повторения символа
        ++frecuency [line[count]];
    }
    for (i = 0; i <= SIZE2; i++) { // упорядочение элем-ов массива методом пузырка
        for (caracter = 32; caracter <= SIZE2 - 1; caracter++) {
            if (frecuency[caracter] < frecuency[caracter + 1]) {
                tempFrec = frecuency[caracter + 1];
                frecuency[caracter + 1] = frecuency[caracter];
                frecuency[caracter] = tempFrec;
                tempSimb = simbols[caracter + 1];
                simbols[caracter + 1] = simbols[caracter];
                simbols[caracter] = tempSimb;
            }
        }
    }

    printf("%s%17s\n", "Frecuency", "character");
    for (i = 32; i < SIZE2; i++) {
        if (frecuency[i] > 0 && simbols[i] != '\0') {
            printf("%6d%17c\n", frecuency[i], simbols[i]);
        }
    }
    return 0;
}
