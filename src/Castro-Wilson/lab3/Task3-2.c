/***************************************************************
 * Программа, которая для введеной строки определяет кол. слов *
 * и выводит каждое слово на отдельной строке и его длину.     *
 * By Wilson Castro. 11.2016.                                  *
 ***************************************************************/
#include <stdio.h>
#include <string.h>
#define SIZE 256
#define TRUE 1
#define FALSE 0

int main() {
    char line[SIZE] = {0};
    int i, j, x;
    int k = 0;
    int cuonter = 0;
    int firstLeter = 0;
    int secondLeter = 0;
    int firstWord = FALSE;
    int secundWord = FALSE;

    printf("Введите строку для обработки\n");
    fgets(line, 256, stdin);
    printf("Введенные слова:\n");
    j = strlen(line);
    line[j - 1] = '\0';

    for (i = 1; i < j; i++) {
        if (i - 1 == 0 && line[i - 1] != ' ') { // пойск первого слово 
            firstWord = TRUE;
            firstLeter = i - 1;
        } else
            if (line[i - 1] == ' ' && line[i] != ' ') { // пойск первого слово если до него есть пробел
            firstWord = TRUE;
            firstLeter = i;
        }
        if (line[i - 1] != ' ' && line[i] == ' ') { //цикл для случая окончаяния слова с пробелом 
            cuonter++;
            secundWord = TRUE;
            secondLeter = i - 1;
        }
        if (line[i - 1] != ' ' && line[i] == '\0') { //цикл для случая окончаяния слова и строка 
            cuonter++;
            secundWord = TRUE;
            secondLeter = i - 1;
        }

        if (firstWord == TRUE && secundWord == TRUE) { // цикл для ввывода каждого слова на отдельной строке и  
            for (k = firstLeter; k <= secondLeter; k++) { // его длуну
                if (!line[k] != ' ') {
                    printf("%c", line[k]);
                    x = (secondLeter - firstLeter) + 1;
                }
            }
            printf("\n");
            printf("Количество букв: %d\n", x);
            firstWord = FALSE;
            secundWord = FALSE;
            firstLeter = 0;
            secondLeter = 0;
        }
    }
    printf("Количество слов: %d\n", cuonter);
    return 0;
}
