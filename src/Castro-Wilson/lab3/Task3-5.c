/****************************************************************
 * Программа, которая формирует целочисленный массив размера N, *
 * а затем находит сумму элементов, расположенным между первым  *
 * отрицательным и последним положительным элементами.          *
 * By Wilson Castro. 12.2016.                                   *
 ****************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#define SIZE 256

int main() {
    int i = 0;
    int N = 0;
    int s = 0;
    int k = 0;
    int randCase;
    int randNum;
    int firstNegativ = 0;
    int lastPositiv = 0;
    int summa = 0;
    int line[SIZE] = {0};
    srand(time(0));
    printf("Please, type quantity elements for the massive\n");
    scanf("%d", &N); //scanf("%f", &stature);
    // k = strlen(line);
    line[k - 1];
    while (i < N) {
        randCase = rand() % 2;
        switch (randCase) {
            case 0: // для отрицательных чисел
                randNum = (rand() % 10);
                line[i] = (-1) * randNum;
                break;
            case 1: // для положительных чисел
                randNum = (rand() % 10);
                line[i] = randNum;
                break;
        }
        i++;
    }
    for (int s = 0; s < N; s++) { // показывает полученный массив
        printf("%d, ", line[s]);
    }
    for (int s = 0; s < N; s++) { //  ищет первое отрицательное число
        if (line[s] < 0) {
            firstNegativ = s;
            break;
        }
    }
    for (int s = N; s >= firstNegativ; s--) { //  ищет последнее  отрицательное число
        if (line[s] > 0) {
            lastPositiv = s;
            break;
        }
    }
    for (int s = firstNegativ + 1; s <=lastPositiv-1; s++) { // осуществляет сумму в соответствии условия
        summa += line[s];
    }
    printf("\nSumma needed interval = %d\n", summa);
    return 0;
}
