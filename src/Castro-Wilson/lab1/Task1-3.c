/*Программа для перевода значения на единицах градусы или радианы
 * by Wilson Castro.
 * 2016
 */

#include <stdio.h>
#include <string.h>

int main(void) {
    float ang;
    char unit;
    printf("Введите значение угла для перевода в градусах или в радианах в формате например 45.00D или 45.00R:\n");
    scanf("%f%c", &ang, &unit);
    if (unit == 'D') {
        printf("Ваш угол в радианах равен: %.2f\n", ang * 0.017);
    }
    if (unit == 'R') {
        printf("Ваш угол в градусах равен: %.2f\n", ang * 57.296);
    } else
        printf("Вы не правильно ввели формат угла.\n");

    return 0;
}