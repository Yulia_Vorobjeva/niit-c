/*
 * Программа выводящая на экран 10 паролей.
 * By Wilson Castro. 11.2016.
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
void password();

int main() {
    int j;
    srand(time(0));
    for (j = 1; j <= 10; j++) {
        password();
        puts("");
    }
    return 0;
}

void password() { // Функция, которая генерирует пароли
    int i = 0;
    int n; // переменная для random
    while (i < 8) {
        n = rand() % 3;
        switch (n) {
            case 0: // случайный порядок от A до Z
                putchar(65 + rand() % (90 + 1 - 65));
                break;
            case 1: // случайный порядок от а до z
                putchar(97 + rand() % (122 + 1 - 97));
                break;
            case 2: // случайный порядок от 0 до 9
                putchar(48 + rand() % (57 + 1 - 48));
                break;
        }
        i++;
    }
}

