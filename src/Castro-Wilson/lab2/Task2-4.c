/*
 * Программа переставляющая символы в массиве согласно правилу:
 * сначала идут латинские буквы, потом цифры.
 * By Wilson Castro. 11.2016.
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define SIZE 63
void gen_aleator();

int main() {
    srand(time(0));
    gen_aleator();
    return 0;
}

void gen_aleator() { //Функция, которая генерирует строку
    int i = 0;
    int n, s, t;
    char line[SIZE] = {0};
    while (i <= 61) {
        n = rand() % 3;
        switch (n) {
            case 0: // case для генерации заглавных букв и ввода в массив
                for (s = 0; s <= 25; s++) {
                    t = (65 + rand() % 26);
                    line[s] = t;
                }
                break;
            case 1: // case для генерации строчных букв и ввода в массив
                for (s = 26; s <= 51; s++) {
                    t = (97 + rand() % 26);
                    line[s] = t;
                }
                break;
            case 2: // case для генерации цифр и ввода в массив
                for (s = 52; s <= 61; s++) {
                    line[s] = t;
                    t = (48 + rand() % 10);
                }
                break;
        }
        i++;
    }
    for (int s = 0; s < 63; s++) {
        printf("%c", line[s]);
    }
}