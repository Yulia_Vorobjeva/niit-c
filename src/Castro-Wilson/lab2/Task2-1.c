/*
 *Программа, имитирующая работу высотометра бомбы.
 *by Wilson Castro. 11.2016 
 */
#include <Stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <windows.h>
#define G 9.8f

int main() {
    time_t now;
    long int now1 = 0;
    long int now2 = 0;
    long int now3 = 0;
    float h = 0.0f; // Высота выброса бомбы
    float t = 0.0f; // Время подения бомбы
    float t2 = 0.0f;
    float h2 = 0.0f;
    float h3 = 0.0f;
    printf("Пожалуйста, введите высоту выброса бомбы\n");
    scanf("%f", &h);
    now1 = time(&now);
    t = sqrt((2 * h) / G);
    while (now3 <= (long int)t- 1) {
        now2 = time(&now);
        now3 = now2 - now1;
        t2 = pow(now3, 2);
        h2 = (G * t2) / 2;
        h3 = h - h2;
         printf("t=%ld с  h=%.2f м\n", now3, h3);
        if (now3 == (long int)t - 1) {
            puts("ВАВАН ВАВАН!!!!");
            break;
        } 
        Sleep(1000);
    }
    return 0;
}
