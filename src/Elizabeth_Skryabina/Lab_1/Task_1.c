#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main()
{
	char sex;
	int height = 0, weight = 0;
	int c;

	float idweight;
	while (1)
	{
		printf("Enter your sex (W or M) ");
		scanf("%c", &sex);
		if ((sex == 'M') || (sex == 'm') || (sex == 'W') || (sex == 'w'))
			break;
		else
		{
			printf("Error!\n");
			do {
				c = getchar();
			} while (c != '\n' && c != EOF);
			continue;
		}
	}
	printf("Enter your height in centimeters ");
	scanf("%d", &height);
	printf("Enter your weight in kilograms ");
	scanf("%d", &weight);
	if ((sex == 'M') || (sex == 'm'))
		idweight = (height - 100)*1.15f;
	else
		idweight = (height - 110)*1.15f;
	printf("Your ideal weight = %5.2f kg\n", idweight);
	if ((weight < (idweight - 1)))
		printf("Get fat!\n");
	else if ((weight < (idweight + 1)) || (weight < (idweight - 1)))
		printf("Normal!\n");
	else
		printf("Grow thin!\n");
	return 0;
}