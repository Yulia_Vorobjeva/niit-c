#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

#define N 10
#define M 80

int main()
{
	char *pstr[N] = { 0 };
	char Text[N][M] = { 0 };
	char *p;
	int i = 0, j, numStr=0,index,nminstr=0;
	printf("Enter the strings:\n");
	while (numStr < N && *fgets(Text[numStr], M, stdin) != '\n')
		pstr[numStr] = Text[numStr++];
	for (i = 0; i < numStr - 1; i++)
	{
		index = i;
		nminstr = 0;
		p=pstr[i];
		for (j = i + 1; j < numStr; j++)
			if (strlen(pstr[j]) < strlen(p))
			{
				p = pstr[j];
				nminstr = j;
			}
		if (nminstr != 0)
		{
			pstr[nminstr] = pstr[i];
			pstr[i] = p;
		}
	}
	printf("\n");
	i = 0;
	while(i < numStr)
		printf(pstr[i++]);
	return 0;
}