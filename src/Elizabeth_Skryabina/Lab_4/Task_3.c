/* palindrome
*/
#include <stdio.h>
#include <string.h>
#define N 256

void EnterText(char str[]);
void Palindrome(char str[]);

int main()
{
	char str[N] = { 0 };
	int Count = 0;
	EnterText(str);
	Palindrome(str);
	return 0;
}

void EnterText(char str[])
{
	printf("Enter your text:\n");
	fgets(str, N, stdin);
	str[strlen(str) - 1] = 0;
}

void Palindrome(char str[])
{
	char *pbegin = str, *pend = &str[strlen(str) - 1];
	int matches = 0, numletters = 0, i = 0;
	while (pbegin < pend)
	{
		while (*pbegin == ' '&& pbegin < pend)
			pbegin++;
		while (*pend == ' '&& pend > pbegin)
			pend--;
		if (*pbegin!=' ' && *pend!=' ' && (*pbegin++ == *pend--))
			matches++;
		else
			break;
	}
	while (str[i])
	{
		if (str[i++] != ' ')
			numletters++;
	}
	if (matches == numletters / 2)
		printf("The line is palindrome!\n");
	else
		printf("The line isn't palindrome!\n");
}