#include <stdio.h>
#include <string.h>
#define SIZE 256

void FillTable(char text[], char simv[], int kolv[]);
void BubSort(int kolv[], char simv[], int kolvsimv);

int main()
{
	char text[SIZE];
	char simv[SIZE] = { 0 };
	int kolv[SIZE] = { 0 };
	int i = 0, kolvsimv = 0;
	printf("Enter text:\n");
	fgets(text, SIZE, stdin);
	text[strlen(text) - 1] = 0;
	FillTable(text, simv, kolv);
	while (simv[i++] != '\0')
		kolvsimv++;
	BubSort(kolv, simv, kolvsimv);
	i = 0;
	while (simv[i] != '\0')
	{
		printf("%c = %d\n", simv[i], kolv[i]);
		i++;
	}
	return 0;
}

void FillTable(char text[], char simv[], int kolv[])
{
	int i, j;
	for (i = 0; text[i]; i++)
	{
		j = 0;
		while (text[i] != simv[j] && simv[j] != '\0')
			j++;
		simv[j] = text[i];
		kolv[j]++;
	}
}

void BubSort(int kolv[], char simv[], int kolvsimv)
{
	int i, j, ci, lastindex;
	char cs;
	i = kolvsimv - 1;
	while (i > 0)
	{
		lastindex = 0;
		for (j = 0; j < i; j++)
			if (kolv[j + 1] > kolv[j])
			{
				ci = kolv[j + 1];
				kolv[j + 1] = kolv[j];
				kolv[j] = ci;
				cs = simv[j + 1];
				simv[j + 1] = simv[j];
				simv[j] = cs;
				lastindex = j;
			}
		i = lastindex;	}	
}
