/*search and print words
*/
#include <stdio.h>
#include <string.h>

int main()
{
	char str[256] = { 0 };
	int i = 0, numword = 0, numletter = 0;
	printf("Enter your text:\n");
	fgets(str, 256, stdin);
	str[strlen(str) - 1] = 0;
	if (str[i] != ' ')
	{
		numword++;
		printf("\n%d  - ", numword);
		while (str[i] != ' '&&str[i])
		{
			printf("%c", str[i++]);
			numletter++;
		}
		printf(" %d", numletter);
	}
	while (str[i])
	{
		numletter = 0;
		if (str[i] == ' ' && str[i] != '\0' && str[++i] != ' '&& str[i] != '\0')
		{
			printf("\n%d  - ", ++numword);
			while (str[i] != ' '&&str[i] != '\0')
			{
				printf("%c", str[i]);
				i++;
				numletter++;
			}
			printf(" %d", numletter);
		}
	}
	printf("\nVsego %d slov\n", numword);
	return 0;
}