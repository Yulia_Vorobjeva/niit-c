/* letters and then numbers
*/
#define _CRT_SECURE_NO_WARNINGS
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#define SIZE 80

int main()
{
	int lengthstr, i, j; //i-position digit; j - position letter
	char simv[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghigklmnopqrstuvwxyz0123456789";
	char str[256] = { 0 };
	char c;
	srand(time(NULL));
	lengthstr = rand() % 100 + 20;
	for (i = 0; i<lengthstr; i++)
		str[i] = simv[rand() % strlen(simv)];
	printf("%s\n", str);
	for (i = 0, j = --lengthstr; i<j; i++, j--)
	{
		while (i<lengthstr && isalpha(str[i]))
			i++;
		while (j >= 0 && isdigit(str[j]))
			j--;
		if (i>j)
			break;
		c = str[i];
		str[i] = str[j];
		str[j] = c;
	}
	printf("%s\n", str);
	return 0;
}