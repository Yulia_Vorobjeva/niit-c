#include <stdio.h>
#define SIZE 256

int main()
{
	char text[SIZE];
	char simv[SIZE] = { 0 };
	int kolv[SIZE] = { 0 };
	int i, j;
	printf("Enter text:\n");
	fgets(text, SIZE, stdin);
	for (i = 0; text[i]; i++)
	{
		j = 0;
		while (text[i] != simv[j] && simv[j] != '\0')
			j++;
		simv[j] = text[i];
		kolv[j]++;
	}
	i = 0;
	while (simv[i + 1] != '\0')
	{
		printf("%c = %d\n", simv[i], kolv[i]);
		i++;
	}
	return 0;
}