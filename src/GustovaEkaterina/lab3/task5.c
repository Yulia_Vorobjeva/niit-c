/*Write a program that creates an integer array of size N, and the
then finds the sum of the elements located between the first and
the last negative positive elements.
*/

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

#define N 80
#define maxborder 21
#define minborder -10

void CreateLane(int *lane);
void FindFirsNegativ(int *lane, int *num);
void FindLastPositiv(int *lane, int *num);
void GetSumBetween(int *lane, int max, int min);

int main()
{
	int Negativ;
	int Positiv;
	int randlane[N];
	srand(time(0));
	CreateLane(randlane);
	FindFirsNegativ(randlane, &Negativ);
	FindLastPositiv(randlane, &Positiv);
	GetSumBetween(randlane, Positiv, Negativ);
	return 0;
}
void CreateLane(int *lane)
{
	int i = 0;
	while(1) 
	{
		lane[i] = rand() % maxborder - minborder;
		i++;
		if (i == N)
			break;
	}
}
void FindFirsNegativ(int *lane, int *num)
{
	(*num) = 0;
	while (lane[(*num)] > 0)
	{
		(*num)++;
	}
}
void FindLastPositiv(int *lane, int *num)
{
	(*num) = N - 1;
	while (lane[(*num)] < 0)
	{
		(*num)--;
	}
}
void GetSumBetween(int *lane, int max, int min)
{
	int sum = 0;
	while (1)
	{
		min++;
		if (min < max)
			sum += lane[min];
		else
			break;
	}
	printf("%d\n", sum);
}