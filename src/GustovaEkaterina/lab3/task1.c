// Write a program to count the number of words in the input string user
#include<stdio.h>
#include<string.h>

#define SIZE 80

void GetLane(char *lane);
void CountingWords(char *lane, int *count);

int main()
{
	int i = 0;
	int count = 0;
	char userlane[SIZE];
	GetLane(userlane);
	CountingWords(userlane,&count);
	printf("%d words.\n", count);
	return 0;
}
void GetLane(char *lane)
{
	printf("Enter the string.\n");
	fgets(lane, SIZE, stdin);
	lane[strlen(lane) - 1] = 0;
}
void CountingWords(char *lane, int *count)
{
	int i = 0;
	while(1)
	{
		if (lane[i])
		{
			if (lane[i] != ' ' && lane[i + 1] == ' ' || lane[i] != ' ' && lane[i + 1] == 0)
			{
				(*count)++;
			}
			i++;
		}
		else
			break;
	}
}