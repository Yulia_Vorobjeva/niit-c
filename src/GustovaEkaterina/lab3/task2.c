/* Write a program that for selected line defines the number of words 
and displays each word on a separate line and length */

#include<stdio.h>
#include<string.h>

#define SIZE 80

void GetLane(char *lane);
void InputWords(char *lane, int *numwords);

int main()
{
	int i = 0;
	int numwords = 0;
	char userlane[SIZE];
	GetLane(userlane);
	InputWords(userlane, &numwords);
	printf("%d words.\n",numwords);
	return 0;
}
void GetLane(char *lane)
{
	printf("Enter the string.\n");
	fgets(lane, SIZE, stdin);
	lane[strlen(lane) - 1] = 0;
}
void InputWords(char *lane, int *numwords)
{
	int i = 0;
	int count = 0;
	for (i = 0; i < strlen(lane); i++)
	{
		if (lane[i] != ' ')
		{
			putchar(lane[i]);
			count++;
			if (lane[i + 1] == ' ' || lane[i + 1] == '\0')
			{
				printf(" %d\n", (count));
				count = 0;
				(*numwords)++;
			}
		}
	}
}
