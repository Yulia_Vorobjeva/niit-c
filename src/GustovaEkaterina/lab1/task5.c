// centered.
#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
#define N 80

int main()
{
	char userlane[N];
	char buf[N];
	int len, quantitySpaces; 
	printf("Enter your massage\n");
	fgets(userlane, N, stdin);
	len = strlen(userlane);
	quantitySpaces = (80 + len) / 2;
	sprintf(buf, "%%%ds", quantitySpaces);
	printf(buf, userlane);
	return 0;
}