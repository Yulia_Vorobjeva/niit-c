/*Change the program to find the longest sequence in the mass
Siwa using pointers instead of the numeric variables.*/

#include <stdio.h>
#include <string.h>

#define SIZE 80

void GetLane(char *lane);
void FindMaxSequence(char *lane);
void InputMax(int flag, char sym);

int main()
{
	char userlane[SIZE];
	GetLane(userlane);
	FindMaxSequence(userlane);
}
void GetLane(char *lane)
{
	printf("Enter your lane.\n");
	fgets(lane, SIZE, stdin);
	lane[strlen(lane) - 1] = 0;
}
void FindMaxSequence(char *lane)
{
	int i = 0;
	int count = 1;
	int max = 0;
	char *sym;
	while (1)
	{
		if (lane[i] == lane[i + 1])
			count++;
		else if (lane[i] != lane[i + 1])
		{
			if (count > max)
			{
				max = count;
				sym = &lane[i];
			}
			count = 1;
		}
		i++;
		if (lane[i] == '\0')
			break;
	}
	printf("%d ", max);
	InputMax(max, *sym);
}
void InputMax(int flag, char sym)
{
	while (1)
	{
		if (flag)
		{
			putchar(sym);
			flag--;
		}
		else
		{
			putchar('\n');
			break;
		}
	}
}