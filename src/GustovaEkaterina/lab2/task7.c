//Write a program that displays a table of symbols occurrence of a string entered by the user

#include <stdio.h>

#define COUNT 256
#define SIZE 80

void GetLane(char *lane);
void FillCounters(char *lane, int *count);
void OutputResult(int *count, int flag);

int main()

{
	char userlane[SIZE];
	int count[COUNT] = {0};
	int flag = 0;
	GetLane(userlane);
	FillCounters(userlane, count);
	OutputResult(count, flag);
	return 0;
}
void GetLane(char *lane)
{
	fgets(lane, SIZE, stdin);
}
void FillCounters(char *lane, int *count)
{
	int i = 0;
	while (lane[i] != '\n')
	{
		count[lane[i++]]++;
	}
}
void OutputResult(int *count, int flag)
{
	if (count[flag])
	{
		printf("%c is found %d times", flag, count[flag]);
		putchar('\n');
	}
	flag++;
	if (flag < COUNT)
		OutputResult(count, flag);
}