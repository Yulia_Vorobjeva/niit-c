//Write a program "Guess the number".

#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#define maxborder 100
#define minborder 1

void GenerationNum(int *num);
void Dialog(int *reference, int *diference);
void RecuestNum(int*usernum);

int main()
{
	int unknownnum;
	int usernum = 0;
	srand(time(NULL));
	GenerationNum(&unknownnum);
	Dialog(&unknownnum, &usernum);
	return 0;
}
void GenerationNum(int *num)
{
	(*num) = rand()%maxborder + minborder;
}
void Dialog(int *reference, int *diference)
{
	const char *tips[] = { "Less","More","Guessed" };
	while (1)
	{
		RecuestNum(diference);
		if ((*diference) > (*reference))
			puts(tips[0]);
		else if ((*diference) < (*reference))
			puts(tips[1]);
		else
		{
			puts(tips[2]);
			break;
		}
	}
}
void RecuestNum(int *usernum)
{
	printf("Enter your version number.\n");
	scanf("%d", usernum);
}
