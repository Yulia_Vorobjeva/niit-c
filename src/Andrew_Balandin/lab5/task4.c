/*
Написать программу, которая читает построчно текстовый файл и
переставляет случайно слова в каждой строке
Замечание:
Программа открывает существующий тектстовый файл и читает его построч-
но. Для каждой строки вызывается функция, разработанная в рамках задачи 1
*/

#include <stdio.h>
#include <string.h>

#define NCHARS 256

void rndWords(char*);

int main()
{
    char buf[NCHARS] = {0};
    char tmp[NCHARS] = {0};
    char* pwords[NCHARS] = {NULL};
    int i, nwords;
    FILE* fpin;
    
    fpin = fopen("in.txt", "rt");
    if(fpin == NULL)
    {
        perror("ERROR");
        return 1;
    }
    srand(time(NULL));
    while(!feof(fpin))
    {
        buf[0] = '\0';
        fgets(buf, NCHARS, fpin);
        rndWords(buf);
        printf(buf);
    }
    printf("\n");
    fclose(fpin);

    return 0;
}