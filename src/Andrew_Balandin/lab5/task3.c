/*
Написать программу, переставляющую случайным образом симво-
лы каждого слова каждой строки текстового файла, кроме первого
и последнего, то есть начало и конец слова меняться не должны.
Замечание:

Программа открывает существующий тектстовый файл и читает его построч-
но. Для каждой строки выполняется разбивка на слова и независимая обра-
ботка каждого слова.
*/



#include <stdio.h>
#include <string.h>

#define NCHARS 256

void rndCharsInStr(char* [], int);
int getWords(const char* , char* []);

int main()
{
    char buf[NCHARS] = {0};
    char* pwords[NCHARS] = {NULL};
    int i, nwords;
    FILE* fpin;
    
    srand(time(NULL));
    fpin = fopen("in.txt", "rt");
    if(fpin == NULL)
    {
        perror("ERROR");
        return 1;
    }
    
    while(!feof(fpin))
    {
        buf[0] = '\0';
        fgets(buf, NCHARS, fpin);
        rndCharsInStr(pwords, getWords(buf, pwords));
        printf(buf);
    }
    printf("\n");
    fclose(fpin);

    return 0;
}