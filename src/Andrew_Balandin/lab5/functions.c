//functions for tasks 1,3,5
#include <string.h>

#define TRUE 1
#define FALSE 0
#define N 256

void printWord(char* pword)
{
    int i = 0;
    while(pword[i] != '\0' && !isspace(pword[i]))
        putchar(pword[i++]);
}

int numWords(const char* str)//OR char** pwords?
{
    int i = 0,
        numwords = 0,
        inword = FALSE;

    for(i = 0; str[i] != '\0'; i++)
    {
        if(!inword && !isspace(str[i]))
        {
            numwords++;
            inword = TRUE;
        }
        if(inword && isspace(str[i]))
            inword = FALSE;
    }
    return numwords;
}

int getWords(const char* str, const char* pwords[])//OR char** pwords?
{
    int i = 0,
        j = 0,
        inword = FALSE;
    
    for(i = 0; str[i] != '\0'; i++)
    {
        if(!inword && !isspace(str[i]))
        {
            pwords[j++] = &str[i];
            inword = TRUE;
        }
        if(inword && isspace(str[i]))
            inword = FALSE;
    }
    return j;
}

int lenWord(const char* pword)
{
    int len = 0;
    while(*(pword + len) != '\0' && !isspace(*(pword + len)))
        len++;
    return len;
}

int delWord(char* pword)
{
    int i = 0;
    int lenword = lenWord(pword);
    while(pword[lenword] != '\0')
        pword[i++] = pword[lenword++];
    pword[i] = '\0';
    return lenword - i;
}

void insStrToWord(char* str, char* pos)
{
    int lstr = strlen(str);
    int lpos = strlen(pos);
    int i;
    while(lpos >= 0)
    {
        pos[lpos + lstr] = pos[lpos];
        lpos--;
    }
    for(i = 0; i < lstr; i++)
        pos[i] = str[i];
}

void copyWordToStr(const char* pwordfrom, char* str)
{
    int i = 0;
    while(pwordfrom[i] != '\0' && !isspace(pwordfrom[i]))
    {
        str[i] = pwordfrom[i];
        i++;
    }
}

void clearStr(char * str)
{
    while(*str != '\0' && !isspace(*str))
        *(str++) = '\0';
}

void swapWords(int nword1, int nword2, char* str)
{
    char tmp1[N] = {0};
    char tmp2[N] = {0};
    char* pwords[N] = {NULL};
    getWords(str, pwords);
    
    copyWordToStr(pwords[nword1], tmp1);
    copyWordToStr(pwords[nword2], tmp2);
    
    delWord(pwords[nword1]);
    insStrToWord(tmp2, pwords[nword1]);

    getWords(str, pwords);

    delWord(pwords[nword2]);
    insStrToWord(tmp1, pwords[nword2]);
}

rndWords(char* str)
{
    int i, j;
    int nwords = numWords(str);
    for(i = 0; i < nwords; i++)
    {
        while((j = (rand(time(NULL)) % nwords)) == i)
            ;
        swapWords(i,j,str);
    }
    
}

void rndCharsInWord(char* pwords)
{
    int i,
        j,
        rnd;
    
    char tmp;
    
    int nchars = lenWord(pwords);
    for(j = 1; j < nchars - 1; j++)
    {
        rnd = rand() % (nchars - 2) + 1;
        tmp = *(pwords + j);
        *(pwords + j) = *(pwords + rnd);
        *(pwords + rnd) = tmp;
    }
}

void rndCharsInStr(char* pwords[], int nwords)
{
    int i;
    for(i = 0; i < nwords; i++)
        rndCharsInWord(pwords[i]);
}
