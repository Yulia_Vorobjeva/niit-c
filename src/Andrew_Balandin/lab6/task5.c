/*
Написать программу, которая измеряет время вычисления N-ого
члена ряда Фибоначчи. Предусмотреть вывод таблицы значений
для N в диапазоне от 1 до 40 (или в другом диапазоне по желанию)
на экран и в файл
*/

#include <stdio.h>
#include <time.h>

#define N 39

long long fib(int n)
{
    if(n <= 1)
        return n;
    else
        return fib(n - 1) + fib(n - 2);
}

int main()
{
    FILE * fp;
    long long i, f;
    long t;
    fp = fopen("fib5.txt", "wt");
    if(fp == NULL)
    {
        perror("FILE: ");
        return 1;
    }
    for(i = 2; i <= N; i++)
    {
        
        t = clock();
        f = fib(i);
        t = clock() - t;
        fprintf(fp, "%lld; %lld; %ld\n", i, f, t);
    }
    //printf("%d \n", fib(8));
    fclose(fp);
    printf("Done!\n");
    return 0;
}