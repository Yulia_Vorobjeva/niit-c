/*
Написать программу, которая формирует в двумерном символьном
массиве фрактальное изображение и выводит его на консоль
*/

#include <stdio.h>
#include <time.h>


#define DEEP 3
#define N 36
#define SPACE ' '
#define STAR '*'


char screen[N][N];

int pow3(int n)
{
    return n <= 0 ? 1 : pow3(n - 1) * 3;
}

void sleep(float t)
{
    t = clock() + t * CLOCKS_PER_SEC;
    while(clock() <= t)
        ;
}
void clear()
{
    int i, j;
    for(i = 0; i < N; i++)
    {
        for(j = 0; j < N; j++)
            screen[i][j] = SPACE;
    }
}

void print()
{
    int i, j;
    for(i = 0; i < N; i++)
    {
        for(j = 0; j < N; j++)
            putchar(screen[i][j]);
        putchar('\n');
    }
}

void draw(int n, int m, int deep)
{
    if(!deep)
    {
        screen[n][m] = STAR;
        return;
    }
    int size = pow3(deep - 1);
    draw(n, m , deep-1);
    draw(n + size, m , deep-1);
    draw(n - size, m , deep-1);
    draw(n, m  + size , deep-1);
    draw(n, m  - size , deep-1);
}

int main()
{
    clear();
    draw( N / 2, N / 2, DEEP);
    print();
    return 0;
}