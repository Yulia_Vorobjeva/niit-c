/*
Написать программу, которая запрашивает количество родственников в семье,
а потом позволяет ввести имя родственника и его возраст. Программа должна
определить самого молодого и самого старого родственника
Замечание:
Нужно завести массив строк для хранения имён и два указателя: young и old,
которые по мере ввода, связывать с нужными строками.
*/

#include <stdio.h>

#define N 10
#define M 80

int main()
{
    unsigned int    nrelatives = 0,
                    age_young = 100000,
                    age_old = 0,
                    age_cur = 0,
                    i = 0;
    
    char*           young,
        *           old;
    
    char            name[N][M] = {0};
    
    printf("Input number of your relatives (under 10): ");
    scanf("%u", &nrelatives);
    
    for(i = 0; i < nrelatives; i++)
    {
        printf("Input name of %u relative and his age over the space: ", i + 1);
        scanf("%s %u", name[i], &age_cur);
        if(age_cur > age_old)
        {
            old = name[i];
            age_old = age_cur;
        }
        if(age_cur < age_young)
        {
            young = name[i];
            age_young = age_cur;
        }
    }
    
    printf("\nYour youngest relative is %s\n", young);
    printf("Your oldest relative is %s\n", old);
    
    return 0;
}