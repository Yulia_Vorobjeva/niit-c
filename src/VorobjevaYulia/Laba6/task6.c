//
//  main.c
//  task6
//
//  Created by Yulia Vorobjeva on 20.01.17.
//  Copyright © 2017 Yulia Vorobjeva. All rights reserved.
//

#include <stdio.h>
typedef unsigned long long ULL;


void fibbonaci(ULL * res, ULL past_res, int i, int N)
{
    ULL tmp = *res;
    
    if(i<=N)
    {
        *res+=past_res;
        fibbonaci(res, tmp, i+1, N);
    }
}

ULL fib(int N)
{
    ULL f = 1;
    
    if (N ==1 || N ==2)
        return 1;
    if(N==0)
        return 0;
    else
    {
        fibbonaci(&f, 0, 2, N);
        return f;
    }
}

int main()
{
    int N;
    printf("enter a nubmer of fibbonacci series element\n");
    scanf("%d", &N);
    printf("result: %lld", fib(N));
    
}
