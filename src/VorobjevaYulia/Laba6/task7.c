//
//  main.c
//  task7
//
//  Created by Yulia Vorobjeva on 11.01.17.
//  Copyright © 2017 Yulia Vorobjeva. All rights reserved.
//

#include <stdio.h>

#define N 9
#define M 27
#define SPACE ' '
#define WALL '#'
#define HUMAN 'X'
#define PATH '.'


char maze[N][M] =
{"###########################",
 "#           #   #         #",
 "##########  #   #         #",
 "#           #   ######  ###",
 "# ######    #             #",
 "#      #    #   ######   ##",
 "#####  #    #   #        ##",
 "       #        #    ######",
 "###########################"
};

int go1[] = {-1,1,0,0};
int go2[] = {0,0,-1,1};

void printMaze()
{
    int i, j;
    for(i=0;i<N;i++)
    {
        for(j=0;j<M;j++)
            putchar(maze[i][j]);
        putchar('\n');
    }
}

int defineThePass(int n, int m)
{
    int i;
    if(n<=0||m<=0||n>=N-1||m>=M-2)
    {
        maze[n][m] = PATH;
        return 1;
    }
    maze[n][m]= PATH;
    for(i=0;i<4;i++)
        if(maze[n+go1[i]][m+go2[i]] == SPACE)
            if(defineThePass(n+go1[i], m +go2[i]))
                return 1;
    return 0;
    
}

void EnterHuman(int *n, int *m)
{
    maze[N/2][M/2] = HUMAN;
    *n = N/2;
    *m = M/2;
}


 int main()
    {
        int n=0, m=0;
        EnterHuman(&n,&m);
        printMaze();
        printf("\n\n\n");
        defineThePass(n, m);
        printMaze();
        return 0;
    }



