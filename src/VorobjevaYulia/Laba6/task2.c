//
//  main.c
//  task2
//
//  Created by Yulia Vorobjeva on 07.01.17.
//  Copyright © 2017 Yulia Vorobjeva. All rights reserved.
//

#include <stdio.h>
typedef long long int  ll;



ll collatz(ll n, ll * i)
{
    (*i)++;
    if(n==1)
        return n;
    if(n%2)
        return collatz(3*n+1,i);
    else
        return collatz(n/2, i);
}


int main()
{
    ll i, max = 0, sequence =0, n = 2;
    for(i=n;i<=1000000;i++)
    {
        sequence = 0;
        collatz(i, &sequence);
        if(sequence>max)
        {
            max = sequence;
            n=i;
        }
    }
    printf("n= %lli\n", n);
    printf("max = %lli\n", max);
    return 0;
}

