//
//  main.c
//  task3
//
//  Created by Yulia Vorobjeva on 08.01.17.
//  Copyright © 2017 Yulia Vorobjeva. All rights reserved.
//

#include <stdio.h>
#define N 256


char * intToChar(int n, char * text)
{
    if(n<0)
    {
        *text++ = '-';
        intToChar(-n,text);
    }
    else if(n>0)
    {
        text = intToChar(n/10, text);
        *text++ = n%10 + '0';
        *text = '\0';
        return text;
    }
    return text;
}

int main()
{
    int n = 0;
    char text[N]={0};
    printf("enter your number\n");
    scanf("%d",&n);
    intToChar(n, text);
    printf("%s",  text);
    printf("\n");
    return 0;
}

