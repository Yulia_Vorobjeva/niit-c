//
//  main.c
//  task4
//
//  Created by Yulia Vorobjeva on 11.01.17.
//  Copyright © 2017 Yulia Vorobjeva. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>


void fillArray( int * mass, const long int n)
{
    int i;
    for(i=0;i<n;i++)
        mass[i]=rand()%10;
}

int sumByIter(int * mass, const long int n)
{
    int i = 0, sum =0;
    for(i=0;i<n;i++)
        sum+=mass[i];
    return sum;
}

int sumByRecur(int * mass, int start, const long int end)
{
    if (end==start)
        return mass[start];
    else return sumByRecur(mass, start, (end+start) / 2)
        + sumByRecur(mass, (end + start) / 2 + 1, end);
}
char * compareTime(const long int time_recur, const long int time_iter)
{
    char * result;
    if(time_recur>time_iter)
        result =  "Time by recursion is bigger\0";
    else result = "time by iterrastion is bigger\0";
    return result;
}
int main()
{
    srand(time(NULL));
    int * mass = 0;
    char * result = 0;
    long int sum_iter, sum_recur,time_iter,time_recur, n;
    printf("enter a deg of 2(it will be a number of arrays elements):\n");
    scanf("%ld", &n);
    mass=malloc(sizeof(int)*n);
    fillArray(mass, n);
    time_iter = clock();
    sum_iter = sumByIter(mass, n);
    time_iter = clock()-time_iter;
    time_recur = clock();
    sum_recur = sumByRecur(mass, 0, n);
    time_recur=clock()-time_recur;
    printf("sum by recur = %ld\n", sum_recur);
    printf("time by recur = %ld\n", time_recur);
    printf("sum by iterration = %ld\n", sum_iter);
    printf("time by itteration = %ld\n", time_iter);
    result = compareTime(time_recur, time_iter);
    printf("%s\n", result);
    free(mass);
    return 0;
    
}
