//programm  tells you "good morning/day/afternoon"
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
int main() {
	setlocale(LC_ALL, "Rus");
	int hours, minutes, seconds;
	char x;
    printf("Enter your time in format "hh:mm:ss"\n");
	scanf("%d%c%d%c%d", &hours,&x,&minutes,&x,&seconds);
		if(hours<0||hours>24||minutes<0||minutes>60||seconds<0||seconds>60||x!= ':'){
			printf("Uncorrect data!\n");
		}
		else
		{
			if(hours<12){
				printf("Good morning!\n");
			}
			else if(hours>=2&&hours<18){
				printf("Good day!\n");
			}
			else printf("Good evening!\n");
		}
	return 0;
}
