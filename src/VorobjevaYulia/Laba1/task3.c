#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#define GtoR 0.0174533
#define RtoG 57.2958

int main(){
	setlocale(LC_ALL,"Rus");
	float ugol;
	char G_or_R;
	printf("Enter your angle value in radians or degrees like  a 45,2G if you have degrees and like a 28,9R if you have radians\n");
	scanf("%f%c", &ugol,&G_or_R);
	if(G_or_R=='G')
	{
		if(ugol>=0&&ugol<=360)
			printf("In radians your angle value is %f\n", ugol*GtoR);
		else 
			printf("Uncorrect data!\n");
	}
	else if(G_or_R=='R')
	{
		if(ugol>=0&&ugol<=6,28319)
			printf("In degrees your angle value is", ugol*RtoG);
		else 
			printf("Uncorrect data\n");
	}
	else
		printf("Uncorrect data!\n");
	return 0;
}
