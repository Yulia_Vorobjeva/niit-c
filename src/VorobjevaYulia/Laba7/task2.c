//
//  main.c
//  task2
//
//  Created by Yulia Vorobjeva on 07.02.17.
//  Copyright © 2017 Yulia Vorobjeva. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 25

struct NODE{
    char word[SIZE];
    int count;
    struct NODE *left;
    struct NODE *right;
};

int max = 0;



struct NODE *makeTree(struct NODE *root, char *word)
{
    if(root == NULL)
    {
        struct NODE *temp = (struct NODE*)malloc(sizeof(struct NODE));
        temp->count = 0;
        strcpy(temp->word, word);
        temp->left=temp->right=NULL;
        return temp;
        
    }
    else if(strcmp(root->word, word)>0)
    {
        root->left = makeTree(root->left, word);
        return root;
    }
    else if(strcmp(root->word, word)<0)
    {
        root->right = makeTree(root->right,word);
        return root;
    }
    else
    {
        root->count++;
        return root;
    }
}


void searchTree(struct NODE * root, char * buf)
{
    if(root)
    {
        searchTree(root->left,buf);
        if(strcmp(root->word, buf)==0)
            root->count++;
        searchTree(root->right, buf);
    }
}

void printTree(struct NODE * root)
{
    if(root)
    {
        printTree(root->left);
        printf("%s - %d\n", root->word, root->count);
        printTree(root->right);
    }
}
int main(int argc, char *argv[]){
    
    struct NODE *root = NULL;
    FILE* fp = fopen("keyWords.txt", "rt");
 
    char  buf[SIZE];
    
    while(!feof(fp))
    {
        fscanf(fp,"%s",buf);
        root = makeTree(root, buf);
    }
    fclose(fp);
    
    fp = fopen(argv[1], "rt");
    while(!feof(fp))
    {
        fscanf(fp,"%s", buf);
        searchTree(root, buf);
    }
    fclose(fp);
    
    printTree(root);
    
    return 0;
   
}
