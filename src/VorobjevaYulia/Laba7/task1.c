//
//  main.c
//  task1
//
//  Created by Yulia Vorobjeva on 05.02.17.
//  Copyright © 2017 Yulia Vorobjeva. All rights reserved.
//



#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 512

typedef struct onecountryinfo{
    char iso[3];
    char fips[3];
    char name [50];
}ONE_COUNTRY;
typedef ONE_COUNTRY * P_ONECOUNTRY;

typedef struct list{
    P_ONECOUNTRY p_onecountry;
    struct list * prev;
    struct list * next;
} LIST;
typedef LIST * P_LIST;
P_LIST create_list(P_ONECOUNTRY one_country);
P_ONECOUNTRY create_one_country(char * string);
P_LIST addToTail(P_LIST tail, P_ONECOUNTRY one_country);
P_LIST find_by_iso(P_LIST head, char * iso_country);
P_LIST find_by_name(P_LIST head, char * name_country);
void print_data(P_LIST p_list);


int main()
{
    char buf[N];
    int count = 0;
    char choise;
    char search[30];
    P_LIST head = NULL;
    P_LIST tail = NULL;
    P_LIST item = NULL;
    FILE *fp = fopen("fips10_4.csv", "rt");
    if(fp==NULL)
    {
        perror("file:");
        exit(1);
    }
    fgets(buf, N, fp);
    while(fgets(buf, N, fp))
    {
       if(count == 0)
       {
           head = create_list(create_one_country(buf));
           tail = head;
       }
        
       else
       {
           tail = addToTail(tail, (create_one_country(buf)));
       }
        count++;
        
    }
    fclose(fp);
    
    printf("Enter '1' if you want find a country by iso\n");
    printf("Enter '2' if your want find a county by its name\n");
    scanf("%c", &choise);
    if(choise == '1')
    {
        printf("Enter country's ISO\n");
        fgets(search, 30, stdin);
        item = find_by_iso(head, search);
        if(item == NULL)
            printf("Not found\n");
        else print_data(item);
    }
    else if(choise == '2')
    {
        printf("Enter a name of country\n");
        fgets(search,30,stdin);
        item = find_by_name(head, search);
        if(item == NULL)
            printf("Not found\n");
        else print_data(item);
    }
    else
        printf("Your data is not correct\n");
    
    
    
}

P_LIST create_list(P_ONECOUNTRY one_country)
{
    P_LIST p_list =(P_LIST)malloc(sizeof(LIST));
    p_list->p_onecountry = one_country;
    p_list->prev=p_list->next=NULL;
    return p_list;
}

P_ONECOUNTRY create_one_country(char * string)
{
    P_ONECOUNTRY one_country = (P_ONECOUNTRY)malloc(sizeof(ONE_COUNTRY));
    int i = 0;
    
    while(*string!= ' '&& *string != ',')
    {
        one_country->iso[i++] = *string++;
    }
    one_country->iso[i]=0;
    i=0;
    string++;
    while(*string!=' ' && *string!= ',')
    {
        one_country->fips[i++] = *string++;
    }
    one_country->fips[i]=0;
    i=0;
    string++;
    while(*string != ' '&& *string!= ',')
    {
        one_country->name[i++] = *string++;
    }
    one_country->name[i]=0;
    return one_country;
}

P_LIST addToTail(P_LIST tail, P_ONECOUNTRY one_country)
{
    P_LIST p_list = create_list(one_country);
    if(tail!= NULL)
    {
        tail->next = p_list;
        p_list->prev = tail;
    }
    return p_list;
}

P_LIST find_by_iso(P_LIST head, char * iso_country)
{
    while(head)
    {
        if(strcmp(head->p_onecountry->iso,iso_country)==0)
            return head;
        head=head->next;
    }
    return 0;
}

P_LIST find_by_name(P_LIST head, char * name_country)
{
    while(head)
    {
        if(strcmp(head->p_onecountry->name,name_country)==0)
            return head;
        head=head->next;
    }
    return 0;
}


void print_data(P_LIST p_list)
{
    if(p_list!= NULL)
    {
        puts(p_list->p_onecountry->iso);
        puts(p_list->p_onecountry->fips);
        puts(p_list->p_onecountry->name);
    }
}

