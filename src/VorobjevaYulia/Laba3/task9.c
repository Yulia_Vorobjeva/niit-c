//program finds a maximal - length sequence
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define N 256
int main(){
    char text[N]={0};
    int  counter = 1, maxCounter = 1, i;
    char maxletter = '\0';
    printf("Enter your text\n");
    fgets(text, N, stdin);
    unsigned long int len = strlen(text);
    text[len - 1] = '\0';
    len--;
    
    for (i = 0; i< len; i++)
    {
        if(text[i] == text[i+1])
            counter++;
        else
        {
            if (counter > maxCounter)
            {
                maxCounter = counter;
                maxletter = text[i];
            }
            counter = 1;
        }
    }
    printf ("%d\n", maxCounter);
    for(i=0; i < maxCounter; i++)
    {
        printf("%c", maxletter);
    }
    printf("\n");
    return 0;
}
