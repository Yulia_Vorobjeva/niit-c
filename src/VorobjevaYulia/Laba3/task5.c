/* programm finds a sum of numbers between first minore number and last major*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int main() {
	srand ( time(NULL) );
	int N, i, sum = 0;
	printf("Enter a size of massive: ");
	scanf("%d", &N);
	int mass[N];
	int firstMinor, lastMajor;
	for(i=0; i<N;i++)
	{
		mass[i] = rand()%200-100;
		printf("%d ", mass[i]);
	}
	for(i=0;i<N;i++)
	{
		if (mass[i]>0)
			lastMajor = i;
	}

	for(i=N-1;i>=0;i--)
	{
		if(mass[i]<0 )
			firstMinor = i;
	}	
	
	for(i=firstMinor;i<=lastMajor;i++)
	{
		sum = sum +mass[i];
	}
	
	printf("Sum of elements : %d", sum);
	return 0;
}
