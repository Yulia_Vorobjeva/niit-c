/* programm finds a sum of numbers int the string*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define N 100


int charToInt(char a)
{
    int newInt = (int)a;
    if(newInt<48 || newInt>57)
        newInt = 48;
    return newInt-48;
    
}


int main(){
    char text[N];
    int sum = 0, commonSum = 0, i;
    int num1, num2, num3, num4;
    printf("enter your text\n");
    fgets(text, N, stdin);
    
    unsigned long int len  = strlen(text);
    text[len - 1] = '\0';
    len = len-1;
    
    for (i=0; i<len; i+=4)
    {
        if (len <=4)
        {
            commonSum = commonSum + atoi(&text[i]);
            break;
        }
        else if (len>4)
        {
            num1 = charToInt(text[i])*1000;
            num2 = charToInt(text[i+1])*100;
            num3= charToInt(text[i+2])*10;
            num4 = charToInt(text[i+3]);
            sum = num1+num2+num3+num4;
            commonSum = commonSum + sum;
        }
    }
    printf("the common sum is %d\n", commonSum);
    return 0;
}
