//programm finds a sum of numbers between minimum and maximum elements
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int main() {
	srand ( time(NULL) );
	int N, i, numMax, numMin, max, min, sum = 0;
	printf("Enter a size of massive: \n");
	scanf("%d", &N);
	int mass[N];
	int firstMinor, lastMajor;
	for(i=0; i<N;i++)
	{
		mass[i] = rand()%200-100;
		printf("%d ", mass[i]);
	}
	max = mass[0];
	min = mass[0];
	
	for(i=0;i<N;i++)
	{
		if (mass[i]>max)
		{
			numMax = i;
			max = mass[i];
		}
		else if(mass[i]<min)
		{
			numMin = i;
			min = mass[i];
		}
	}
	
	if (numMax>numMin)
	{
		for(i=numMin;i<=numMax;i++)
			sum = sum + mass[i];
	}
	else if(numMax<numMin)
	{
		for(i=numMax;i<=numMin;i++)
			sum = sum + mass[i];
	}
	
	printf("\nNumber of minimum %d , of maximum %d, sum %d\n", numMin, numMax, sum);
		
	return 0;
	
}
