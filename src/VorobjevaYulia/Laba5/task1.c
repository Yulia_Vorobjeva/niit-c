//
//  main.c
//  task1
//
//  Created by Yulia Vorobjeva on 20.12.16.
//  Copyright © 2016 Yulia Vorobjeva. All rights reserved.
//

/*Написать программу, которая принимает от пользователя строку и выводит ее на экран, перемешав слова в случайном порядке.
Замечание:
Программа должна состоять минимум из трех функций:
a) printWord - выводит слово из строки (до конца строки или пробела) b) getWords - заполняет массив указателей адресами первых букв слов
c) main - основная функция
*/

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

#define N 256


int getWords(char text[N], char * firstLetters[N/2], int len)
{
    int i,j=0, countWords =0;

    for(i=1;i<len;i++)
    {
        if(text[0]!= ' '&& text[0]!='\0')
         {
         firstLetters[0] = &text[0];
         }
        if (text[i]!= ' '&& text[i-1]== ' ')
        {
            firstLetters[j] = &text[i];
        }
        
        if(text[i] != ' '&&(text[i+1]== ' '||text[i+1]=='\0'))
        {
            countWords++;
            j++;
        }
    }
    return countWords;
}

void randIndex(int * indexes, const int countWords)
{
    int i, left =0, right = 0, temp = 0;
    for(i=0;i<countWords;i++)
        indexes[i]=i;
    for(i=0;i<countWords;i++)
    {
        do
            {
                left=rand( )%countWords;
                right=rand( )%countWords;
            }
        while(left==right);
        
        temp=indexes[left];
        indexes[left]=indexes[right];
        indexes[right]=temp;
    }
}


void printWords(char * text, char ** firstLetters, int * indexes, const int countWords)
{
    int i, j;
    for(i=0;i<countWords;i++)
    {
        j=indexes[i];
        while(*firstLetters[j]&&*firstLetters[j]!=' ')
            printf("%c",*firstLetters[j]++);
        putchar(' ');
    }
    printf("\n");
}



int main()
{
    srand(time(NULL));
    char text[N]={0};
    char * firstLetters[N/2]={0};
    int indexes[N/2]={0};
    int countWords= 0;
    printf("Enter your line:\n");
    fgets(text, N, stdin);
    int len = strlen(text)-1;
    text[strlen(text)-1]=0;
    countWords = getWords(text, firstLetters, len);
    randIndex(indexes, countWords);
    printWords(text, firstLetters,indexes,countWords);
    return 0;
}
