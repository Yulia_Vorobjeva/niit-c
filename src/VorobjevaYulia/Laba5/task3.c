//
//  main.c
//  task3
//
//  Created by Yulia Vorobjeva on 03.01.17.
//  Copyright © 2017 Yulia Vorobjeva. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define N 256

void bufRand(char *buf, int lenWord)
{
    int i, left = 0, right = 0, temp = 0;
    
    if(lenWord>3)
    {
        for(i=0; i<lenWord; i++)
        {
            do
            {
                left = rand()%(lenWord-2)+1;
                right = rand()%(lenWord-2)+1;
            }
            while(left==right);
            temp = buf[left];
            buf[left]=buf[right];
            buf[right]=temp;
        }
    }
}


void printWord(char *text, int lenText)
{
    int i=0, j=0, lenWord = 0;
    char buf[N]={0};
    
    while(i<lenText)
    {
        lenWord=0;
        j=0;
        while(text[i]!= ' '&& text[i]!='\0')
        {
            buf[j]=text[i];
            lenWord++;
            i++;
            j++;
        }
        
        bufRand(buf, lenWord);
        
        for(j=0;j<lenWord;j++)
            printf("%c",buf[j]);
        
        putchar(' ');
        i++;
    }
    printf("\n");
}


int main()
{
    srand(time(NULL));
    char text[N];
    int lenText;
    
    FILE *fp = fopen("input.txt", "rt");
    if(fp == NULL)
    {
        perror("file:");
        return 1;
    }
    
    while(fgets(text,sizeof(text),fp))
    {
        lenText = strlen(text)-1;
        text[strlen(text)-1]=0;
        
        printWord(text, lenText);
    }
    printf("\n");
    fclose(fp);
    return 0;
    

}
