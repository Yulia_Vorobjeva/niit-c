
#include <stdio.h>
#define N 100
int main()
{
    FILE *fp;
    int i,j;
    char text[N][N];
    
    fp = fopen("text.txt", "rt");
    if (fp ==NULL)
    {
        perror("File\n");
        return 1;
    }
    
    for(i=0; i<N;i++)
    {
        fgets(text[i], N, fp);
    }
    fclose(fp);
    
    fp = fopen("text.txt", "wt");
    if (fp == NULL)
    {
        perror ("file\n");
        return 1;
    }
    for(i=N; i>0; i--)
        for(j=0; j<N; j++)
            if(strlen(text[j]) == i)
                fputs(text[j], fp);
    fclose(fp);
    return 0;
    
}
