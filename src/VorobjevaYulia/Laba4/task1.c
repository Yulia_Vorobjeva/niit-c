/*Программа, выводящая строки в порядке убывания длины*/

#include <stdio.h>
#define N 20
#define M 100

int main()
{
    char text[N][M];
    int i, count = 0;
    
    printf("Enter your lines:\n");
    
    for(count < N; (*fgets(text[count],M,stdin)!='\n'); count++)
        text[count][strlen(text[count]) - 1] = 0;
    
    for (i=M; i>0; i--)
        for(count = 0; count < N; count ++)
            if(i==strlen(text[count]))
                puts(text[count]);
    
    return 0;
}

        

