//programm writes a table of occurent of symbols in the string
#include <stdio.h>
#include <string.h>
 
#define N 256
int main()
{
    char str[N];
    int count[N] = {0};
    int i=0;

    puts("Enter a string:");
    fgets(str, N, stdin);
    str [strlen(str)-1] = 0;
 
    do
        count[str[i++]]++;
        while(str[i]);
 
    for(i=0;i<N;i++)
    {
        if(count[i] != 0)
            printf("%c = %d \n", i, count[i]);
    }
    return 0;
}
