/*3. �������� ���������, ������� ����������� ������ � ����������, �� ��������
�� ������ ����������� (��������� �������� � ����� ������� � ������ ������)
���������: ���� ������ - ��������� ��������� ��� �������� ������������ ������ � ����
������. */

#include<stdio.h>
#include<string.h>
#define SIZE_STR 256

int main()
{
	int len=0;
	char ent_str[SIZE_STR];
	char *left, *right;

	printf("Please, enter string you want to palindrome check\n");
	fgets(ent_str, SIZE_STR, stdin);
	len = strlen(ent_str)-1; // '\n'
	left = &ent_str[0];
	right = &ent_str[len-1];
	while(left <= right)
	{
		while(*left == ' ')
			left++;
		while(*right == ' ')
			right--;
		if(*left == *right)
		{
			left++;
			right--;
		}
		else
			break;
	}
	if(left >= right)
		printf("Your string - palindrome!!!  ;)\n");
	else
		printf("Your string - not palindrome.   :(\n");
	return 0;
}