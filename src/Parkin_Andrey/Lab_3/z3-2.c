/*2. �������� ���������, ������� ��� �������� ������ ���������� ��-
�������� ���� � ������� ������ ����� �� ��������� ������ � ���
�����
���������: ����� ����������� ����� ����������� ��������.*/
#include<stdio.h>
#define N 256

int main()
{
	int words=0, i=0, j=0;
	char ent_arr[N], word[N];
	printf("Please, enter string (max length %d symbols):\n", N-1);
	fgets(ent_arr, 256, stdin);
	while(ent_arr[i]!='\0')
	{
		if(ent_arr[i]!=' ' && ent_arr[i]!='\t' && ent_arr[i]!='\0' && ent_arr[i]!='\n')
		{
			j=0;
			words++;
			do
			{
				word[j] = ent_arr[i];
				j++;
				i++;
			} while(ent_arr[i]!=' ' && ent_arr[i]!='\t' && ent_arr[i]!='\0' && ent_arr[i]!='\n');
			word[j] = '\0';
			printf("%.2d. %s - %d symbols\n", words, word, j);
		}
		else
			i++;
	}
	printf("Words count = %d\n", words);
	return 0;
}