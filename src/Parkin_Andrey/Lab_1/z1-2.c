/*2. �������� ���������, ������� ����������� ������� ����� � ���-
���� ��:��:��, � ����� ������� ����������� � ����������� ��
���������� ������� ("������ ���� "������ ����"� �.�.)*/
#include <stdio.h>
#define N 13

int main()
{
	int h, m, s, greet;
	char greeting[][N] = {"Good ","night!","morning!","afternoon!","evening!"};
	do
	{
		printf("Enter time in format HH:MM:SS\n");
		scanf("%d:%d:%d", &h, &m, &s);
		if(h < 0 || h > 24) {
			printf("Wrong hours (from 0 to 24)\n");
		} else if (m < 0 || m > 60) {
			printf("Wrong minutes (from 0 to 60)\n");
		} else if (s < 0 || s > 60) {
			printf("Wrong seconds (from 0 to 60)\n");
		} else {
			break;
		}
	}
	while(1);
	// ����������� � ����������� �� �������
	if (h >= 0 && h < 6)
		greet=1;
	else if (h >= 6 && h < 12)
		greet=2;
	else if (h >= 12 && h < 18)
		greet=3;
	else 
		greet=4;
	printf("%s%s\n", greeting[0],greeting[greet]);
	return 0;
}