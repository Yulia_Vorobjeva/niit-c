/*
 ============================================================================
 Name        : lab2.3c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description : Program prints the stars
 ============================================================================
 */
#include <stdio.h>
#include <string.h>

#define MAX_SYMBOLS 49
#define MAX_STRING 24

int number=0;

void converge(char*targ,char*src)
{
   printf ("%s\n",targ);

   int len=strlen(src);
   int middle=len/2;
   int i;
   int j;
   int k;

   for(i=middle,j=middle, k=number; i>0, j<len, k>0;i--,j++, k--)
   {
      targ[i]=src[i];
      targ[j]=src[j];
      printf("%s\n",targ);
   }
}
int main()
{
   char src [MAX_SYMBOLS]="***********************************************";
   char targ[MAX_SYMBOLS]="                                               ";

   setlinebuf(stdout);

   printf("Enter number from 0 to 24\n");
   scanf("%d",&number);

   if(number<MAX_STRING && number>0)
   {
      converge (targ,src);
   }
   else
   {
      printf ("Incorrect value\n");
   }
   return 0;
}


