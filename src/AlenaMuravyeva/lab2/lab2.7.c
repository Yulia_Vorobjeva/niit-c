/*
 ============================================================================
 Name        : lab2.7.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description :Programs determinate number of symbol in the string
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include<string.h>

#define ARR_SIZE 256
#define MAX_LETTERS 80
#define START_SYMBOL 32

int main()
{
	unsigned char ASCII [ARR_SIZE];
	int counters [ARR_SIZE];
	char string[MAX_LETTERS];
	int i=0;

	for(i=0;i<ARR_SIZE;i++)
	{
	   counters [i]=0;
	}
	setlinebuf(stdout);

	printf ("Enter string\n");
	gets(string);

	for(i=0;i<ARR_SIZE;i++)
	{
	   ASCII[i]=i;
	}
	int len=strlen(string);

	for(i=0;i<len;i++)
	{
	   int num=(int)string[i];
	   counters[num]++;
	}
	for(i=START_SYMBOL;i<ARR_SIZE;i++)
	{
	   printf("%5c %9d\n",ASCII[i], counters[i]);
	}
	return 0;
}
