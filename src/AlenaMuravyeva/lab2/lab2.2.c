/*
 ============================================================================
 Name        : lab 2.2.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description : Guess the number
 ============================================================================
 */

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main()
{
   int hiddenNumber=0;
   int userNumbers=-1;

   setlinebuf(stdout);

   srand (time (0));
   hiddenNumber= rand() % 100;

   while (1)
   {
      printf("Please enter number: ");
      scanf("%d", &userNumbers);

      if(userNumbers < hiddenNumber)
      {
         printf("Greater, try again\n");
      }
      else if(userNumbers > hiddenNumber)
      {
         printf("Less, try again\n");
      }
      else
      {
         printf("Congratulations, you won! Hidden number is %d", hiddenNumber);
         break;
      }
   }
  return 0;
}
