/*
 ============================================================================
 Name        : lab5.3.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description :The program permutation random letters in word except first and last
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define MAX_LETTERS 256
#define NUM_OF_PERMUTATIONS 3

enum Boolean{false,true};
typedef enum Boolean BOOL;
typedef struct NextWordFromBuffer NextWordFromBuffer;
typedef struct RandPair RandPair;

struct NextWordFromBuffer
{
   char* pFirstLetter;
   char* pSecondLetter;
   int lenght;
};

struct RandPair
{
   unsigned int randNumber1;
   unsigned int randNumber2;
};

FILE* in;
FILE* out;
char nextString[MAX_LETTERS]={"\0"};
NextWordFromBuffer nextWord;
int savedIndex=0;
int lenBuffString=0;

//forward declaration
BOOL openDefinedFiles();
void closeDefinedFiles();
void readNextStrToBuffer();
BOOL endOfString();
void getNextWordFromBuffer();
void doWordLettersPermutation();
RandPair generateRandPair();
void writeBufferToResultFile();
void cleanBuffer();

int main()
{
   if( openDefinedFiles() == false)
   {
      closeDefinedFiles();
      return -1;
   }

   while(!feof(in))
   {
      readNextStrToBuffer();
      while(!endOfString())
      {
         getNextWordFromBuffer();
         if(nextWord.lenght > 3)
         {
            doWordLettersPermutation();
         }
      }
      writeBufferToResultFile();
      cleanBuffer();
   }

   closeDefinedFiles();
   return 0;
}

BOOL openDefinedFiles()
{
   BOOL result=true;

   in=fopen("text.txt","rt");
   if(in==NULL)
   {
      perror("File:");
      result = false;
   }

   out=fopen("text_out.txt","wt");
   if(out==NULL)
   {
      perror("File:");
      result = false;
   }

   return result;
}

void closeDefinedFiles()
{
   if(in != NULL)
   {
      fclose(in);
   }

   if(out != NULL)
   {
      fclose(out);
   }
}

void readNextStrToBuffer()
{
   fgets(nextString,MAX_LETTERS,in);
   lenBuffString=strlen(nextString);
   if(lenBuffString>1)
   {
      nextString[lenBuffString-1]='\0';
      lenBuffString--;
   }
}
BOOL endOfString()
{
   if(savedIndex>=lenBuffString)
   {
      return true;
   }
   else
   {
      return false;
   }
}
void getNextWordFromBuffer()
{
   int firstLeterIndex=0;
   int secondLeterIndex=0;
   int firstFound=0;
   int secondFound=0;

   for(int i=savedIndex;i<lenBuffString;i++)
   {
      if(i==0 && nextString[i]!=' ')
      {
         firstFound = true;
         firstLeterIndex=i;
         nextWord.pFirstLetter= &nextString[firstLeterIndex];
      }
      else if(nextString[i+1]!=' ' && nextString[i]==' ')
      {
         firstFound = true;
         firstLeterIndex=i+1;
         nextWord.pFirstLetter= &nextString[firstLeterIndex];
      }
      if ((nextString[i]!=' ' && nextString[i+1]==' ')|| (nextString[i]!=' ' && nextString[i+1]=='\0'))
      {
         secondFound = true;
         secondLeterIndex=i;
         nextWord.pSecondLetter=&nextString[secondLeterIndex];
      }

      if (firstFound == true && secondFound == true)
      {
         nextWord.lenght = secondLeterIndex - firstLeterIndex+1;
         savedIndex=secondLeterIndex+1;
         break;
      }
   }
}

void doWordLettersPermutation()
{
   char tmp;
   char *currentPointer=nextWord.pFirstLetter;
   RandPair nextPair;
   while(currentPointer != nextWord.pSecondLetter)
   {
      currentPointer++;
      for(int i=0;i<NUM_OF_PERMUTATIONS;i++)
      {
         nextPair=generateRandPair();
         tmp= nextWord.pFirstLetter[nextPair.randNumber1];
         nextWord.pFirstLetter[nextPair.randNumber1]=nextWord.pFirstLetter[nextPair.randNumber2];
         nextWord.pFirstLetter[nextPair.randNumber2]=tmp;
      }
   }
}

RandPair generateRandPair()
{
   RandPair pair;

   int maxSortedIndexes = nextWord.lenght-1;

   srand(time(0));
   pair.randNumber1=rand()%(maxSortedIndexes);
   pair.randNumber2=rand()%(maxSortedIndexes);

   //don't sort first and last letters
   pair.randNumber1==0?pair.randNumber1++:pair.randNumber1;
   pair.randNumber2==0?pair.randNumber2++:pair.randNumber2;
   pair.randNumber1==maxSortedIndexes?pair.randNumber1-=1:pair.randNumber1;
   pair.randNumber2==maxSortedIndexes?pair.randNumber2-=1:pair.randNumber2;

   return pair;
}

void writeBufferToResultFile()
{
   fputs(nextString,out);
   fputs("\n",out);
   fflush(out);
}

void cleanBuffer()
{
   int i=0;
   for (i=0;i<lenBuffString;i++)
   {
      nextString[i]='\0';
   }
   savedIndex=0;
}
