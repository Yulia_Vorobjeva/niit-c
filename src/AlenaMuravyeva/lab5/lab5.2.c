/*
 ============================================================================
 Name        : lab5.2.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description : Kaleidoscope
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>

#define ARR_SIZE 20
#define ARR_HALF ARR_SIZE/2

char arr[ARR_SIZE][ARR_SIZE]={0};

void cleanArray()
{
   for(int i=0;i<ARR_SIZE;i++)
      {
         for(int j=0; j<ARR_SIZE;j++)
         {
            arr[i][j]=' ';
         }
      }
}

void generateStars()
{
   int number1=0;
   int number2=0;

   for(int i=0;i<ARR_HALF;i++)
   {
      number1=rand()%ARR_HALF;
      number2=rand()%ARR_HALF;
      arr[number1][number2]='*';
   }
}
void copyStars()
{
   char tmp;
   for(int i=0;i<ARR_HALF;i++)
   {
      for(int j=0; j<ARR_HALF;j++)
      {
         tmp = arr[i][j];
         arr[i+ARR_HALF][j+ARR_HALF]=tmp;
         arr[i+ARR_HALF][j]=tmp;
         arr[i][j+ARR_HALF]=tmp;
      }
   }
}
void printArray ()
{
   int i,j;
   for(i=0;i<ARR_SIZE;i++)
   {
      for(j=0; j<ARR_SIZE;j++)
      {
         printf( "%c" ,arr[i][j]);
      }
      printf("\n");
   }
}


int main()
{
   setlinebuf(stdout);

   while(1)
   {
      cleanArray();
      srand(time(0));
      generateStars();
      copyStars();

      system("cls");

      printArray();

      Sleep(3000);
   }
   return 0;
}
