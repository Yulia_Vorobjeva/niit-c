/*
 ============================================================================
 Name        : lab3.6.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description :The program fills an array with random numbers and finds the
              sum between the minimum and maximum values
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define ARR_SIZE 256
#define MAX_RAND_NUMBER 300

int main()
{
   int arrN[ARR_SIZE]={0};
   int min=300;
   int max=0;
   int sum=0;
   int minIndex=0;
   int maxIndex=0;

   setlinebuf(stdout);

   srand(time (0));

   for(int i=0;i<ARR_SIZE;i++)
   {
      arrN[i]=rand()% MAX_RAND_NUMBER;
   }
   for(int i=0;i<ARR_SIZE;i++)
   {
      if(arrN[i]>max)
      {
         max=arrN[i];
         maxIndex=i;
      }
      if(arrN[i]<min)
      {
         min=arrN[i];
         minIndex=i;
      }
   }
   if(minIndex<maxIndex)
   {
      for(int i=minIndex;i<=maxIndex;i++)
      {
         sum=sum+arrN[i];
      }
   }
   else if(minIndex>maxIndex)
   {
      for(int i=maxIndex;i<=minIndex;i++)
      {
         sum=sum+arrN[i];
      }
   }
   printf("Generated Array: ");
   for(int i=0;i<ARR_SIZE;i++)
   {
      printf("%d ",arrN[i]);
   }
   printf("\nSum between %d and %d  = %d\n",min,max,sum);
   return 0;
}
