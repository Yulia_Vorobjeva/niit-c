/*
 ============================================================================
 Name        : lab3.1.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description :Programs believes the number words
 ============================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ARR_SIZE 256

int main()

{
   char string [ARR_SIZE];
   int counterWords=0;

   setlinebuf(stdout);

   printf("Enter string\n");
   fgets(string,ARR_SIZE,stdin);

   int len=strlen(string);
   //remove \r and \n
   string[len-2] = '\0';
   len=len-2;

   for(int i=0;i<len;i++)
   {
      if(string[i]!=' ' && string[i+1]==' ')
      {
         counterWords++;
      }
      else if(string[i]!=' ' && string[i+1]=='\0')
      {
         counterWords++;
      }
   }

   printf("Words:%d\n",counterWords);
   return 0;
}
