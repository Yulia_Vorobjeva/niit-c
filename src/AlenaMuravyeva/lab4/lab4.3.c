/*
 ============================================================================
 Name        : lab4.3.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description :The program defines a palindrome or not
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include<string.h>

#define MAX_LETTERS 80

int main()
{
   char userString[MAX_LETTERS];
   int isPalindrome=0;

   setlinebuf(stdout);

   puts("Enter string");
   fgets(userString,MAX_LETTERS,stdin);

   int len=strlen(userString);
   userString[len-2]='\0';
   len-=2;

   char *p=userString;
   char *ptr=userString + strlen(userString)-1;

   while(p<ptr)
   {
      if (*p!=*ptr)
      {
         puts("This is not a palindrome");
         break;
      }
      else
      {
         isPalindrome=1;
      }
      p++;
      ptr--;
   }

   if(isPalindrome)
   {
      puts("This is a palindrome");
   }

   return 0;
}
