/*
 ============================================================================
 Name        : lab 1.5.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description : Program prints string at center
 ============================================================================
 */

#include <stdio.h>
#include <string.h>

#define ARR_SIZE 256
#define STRING_MIDLLE 40

int main()
{
   char str [ARR_SIZE];

   setlinebuf(stdout);

   printf("Enter string:\n");
   fgets(str,ARR_SIZE,stdin);

   size_t len = strlen(str);

   int rightBorder = STRING_MIDLLE+len/2;

   char format[ARR_SIZE];

   sprintf(format,"%c%ds",'%', rightBorder);
   printf(format, str);

   return 0;
}
