/*
 ============================================================================
 Name        : lab1.3.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   :
 Description : The program  transforms degrees to radians and Vice versa
 ============================================================================
 */


#include <stdio.h>

#define PI 3.1415f

int main()
{
   char letter='D';
   float value =0;

   setlinebuf(stdout);

   printf("Hello.Enter the  value of the angle\n");
   scanf("%f %c",&value,&letter);

   if(letter=='D')
   {
      letter='R';
      printf("%.2f%c\n", (value*PI)/180, letter);
   }
   else if (letter=='R')
   {
      letter='D';
      printf("%.2f%c\n", (value*180)/PI, letter);
   }
   else
   {
      printf("Error. Enter, pleas correct data\n ");
   }
   return 0;
}
