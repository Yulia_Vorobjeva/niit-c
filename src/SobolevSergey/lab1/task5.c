/* Практикум 1. Задача 5. 
Написать программу, которая принимает строку от пользователя и
выводит её на экран, выравнивая по центру.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define N 80

int main()
{
	char string[N];
	
	printf("INPUT string (max 79 characters):\n");
	fgets(string, N, stdin);
	printf("%*s\n", ((N - strlen(string)) / 2) + strlen(string), string);
	
	return 0;
}