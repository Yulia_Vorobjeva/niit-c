/* Практикум 4. Задача 5.
Написать программу, сортирующую строки (см. задачу 1), но использующую
строки, прочитанные из текстового файла. Результат работы программы также
записывается в файл.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

#define N 3
#define M 80

int main( )
{
	FILE *fp;
	char string[N][M]={0};
	char* pstr[N];
	char* tmp;
	int i,j;

	fp=fopen("input.txt","rt");
	if(fp==NULL)
	{
		perror("File:");
		return 1;
	}

	for(i=0; i < N; i++)
		fgets(string[i],M,fp);

	for(i=0; i < N; pstr[i]=string[i],i++);

	fp=fopen("output.txt","wt");
	if(fp==NULL)
	{
		perror("File:");
		return 1;
	}

	for(i=0; i < N; i++)
		for(j=i; j < N; j++)
			if(strlen(pstr[i]) > strlen(pstr[j]))
			{
				tmp=pstr[i];
				pstr[i]=pstr[j];
				pstr[j]=tmp;
			}
	
	for(i=0; i < N; i++)
		fputs(pstr[i],fp);

	printf("Ok! These strings are sorted and written to a file. \n");

	fclose(fp);
	fclose(fp);

	return 0;
}