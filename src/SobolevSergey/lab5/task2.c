/* Практикум 5. Задача 2.
Написать программу ”Калейдоскоп”, выводящую на экран изобра-
жение, составленное из симметрично расположенных звездочек ’*’.
Изображение формируется в двумерном символьном массиве, в од-
ной его части и симметрично копируется в остальные его части.
Замечание:
Решение задачи протекает в виде следующей последовательности шагов:
1) Очистка массива (заполнение пробелами)
2) Формирование случайным образом верхнего левого квадранта (занесение
’*’)
3) Копирование символов в другие квадранты массива
4) Очистка экрана
5) Вывод массива на экран (построчно)
6) Временная задержка
7) Переход к шагу 1.
*/

#define _CRT_SECURE_NO_WARNINGS
# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <time.h>
#include <windows.h>

#define N 25
#define M 60
#define VOID ' ' 
#define SYMBOL '*' 
#define PAUSE 3000

void clearArr(char(*arr)[M]) // 1) Очистка массива (заполнение пробелами)
{
	int i,j;
	for(i=0; i < N; i++)
		for(j=0; j < M; j++)
			arr[i][j]=VOID;
}

//2) Формирование случайным образом верхнего левого квадранта (занесение ’*’)
void UpLeftArr(char(*arr)[M])
{
	int i,j;
	for(i=0; i < N/2; i++)
		for(j=0; j < M/2; j++)
			arr[i][j]=(rand( )%2) ? VOID : SYMBOL;
}

//3) Копирование символов в другие квадранты массива
void copy_Up_Down_Left_Right_Arr(char(*arr)[M])
{
	int i,j;
	for(i=0; i<N/2; i++)
		for(j=0; j<M/2; j++)
			if(arr[i][j]==SYMBOL)					//up left
			{
				arr[i][M-1-j]=SYMBOL;				//up right
				arr[N-1-i][M-1-j]=SYMBOL;			//down right
				arr[N-1-i][j]=SYMBOL;				//down left
			}
}

void printArr(const char(*arr)[M]) //5) Вывод массива на экран (построчно)
{
	int i,j;
	for(i=0; i < N; i++)
	{
		for(j=0; j < M; j++)
			putchar(arr[i][j]);
		putchar('\n');
	}
	printf("\nTo exit the program, press Ctr + C\n");
}

void pause(int milliseconds) //6) Временная задержка
{
	Sleep(milliseconds);
}

int main( )
{
	char arr[N][M];
	srand(time(NULL));

	while(1)
	{
		clearArr(arr);
		UpLeftArr(arr);
		copy_Up_Down_Left_Right_Arr(arr);
		system("cls");							//4. Очистка экрана
		printArr(arr);
		pause(PAUSE);
	}
	return 0;
}