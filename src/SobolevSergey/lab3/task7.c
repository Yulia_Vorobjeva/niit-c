/* Практикум №3. Задача 7.
7. Написать программу, которая печатает таблицу встречаемости символов для
введённой строки, отсортированную по убыванию частоты
Замечание:
Таблица выводится таким образом, что сначала идут самые многочисленные
символы, а затем по мере убывания.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

#define N 256

int main( )
{
	char string[N],symbol_repetitions[N]={0};
	int count[N]={0};
	int i,j,len;

	puts("Enter a string:");
	fgets(string,N,stdin);
	string[strlen(string)-1]=0;
	len=strlen(string);

	for(i=0;i<len;i++)
		for(j=0;j<len;j++)
		{
			if(string[i]==symbol_repetitions[j])
			{
				count[j]++;
				break;
			}
			if(symbol_repetitions[j]==0)
			{
				symbol_repetitions[j]=string[i];
				count[j]++;
				break;
			}
		}

	for(i=N;i>0;i--)
		for(j=0;count[j]>0;j++)
			if(count[j]==i)
				printf("%c - %d \n",symbol_repetitions[j],count[j]);
		
	return 0;
}
