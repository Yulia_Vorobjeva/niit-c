/* Практикум 3. Задача 6.
 Написать программу, которая формирует целочисленный массив размера N,
а затем находит сумму элементов между минимальным и максимальным эле-
ментами.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

#define N 10

int main( )
{
	int string[N];
	int i,ind_min=0,ind_max=0,sum=0;;
	srand(time(NULL));

	for(i=0; i<N; i++)
		string[i]=rand()%9+1;
	
	for(i=0; i<N; i++)
		printf("%d ",string[i]);
	printf("\n");

	for(i=0; i < N; ++i)
		if(string[ind_min] > string[i])
			ind_min=i;
	printf("Minimum element: %d to i=%d\n",string[ind_min],ind_min);

	for(i=0; i < N; ++i)
		if(string[ind_max] < string[i])
			ind_max=i;
	printf("Maximum  element: %d to i=%d\n",string[ind_max],ind_max);

	if(ind_min < ind_max)
	{
		for(i=ind_min+1; i < ind_max; ++i)
			sum+=string[i];
	}
	else if(ind_min > ind_max)
	{
		for(i=ind_max+1; i < ind_min; ++i)
			sum+=string[i];
	}

	printf("Sum of elements: %d\n",sum);

	return 0;
}
